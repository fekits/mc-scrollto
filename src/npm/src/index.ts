/* eslint-disable indent */
import { isString, isNumber, isElement, isFunction } from '@fekit/utils';
const raf = (() => {
  const win: any = typeof window !== 'undefined' ? window : {};
  return (
    win.requestAnimationFrame ||
    win.webkitRequestAnimationFrame ||
    win.mozRequestAnimationFrame ||
    function(callback = () => {}) {
      setTimeout(callback, 1000 / 60);
    }
  );
})();
// 获取元素位置
const _pos = function(elements: any, el: any) {
  let top = elements.offsetTop;
  let left = elements.offsetLeft;
  let parent = elements.offsetParent;
  while (parent != null && parent !== el) {
    top += parent.offsetTop;
    left += parent.offsetLeft;
    parent = parent.offsetParent;
  }
  return { x: left, y: top };
};

/**
 * 滚动条插件
 *
 * @param param.el         {String}     指定滚动条所在的容器
 * @param param.to         {*}          指定需要滚动的位置（可以是数字比如100，也可以是需要滚动到的DOM元素位置比如"#f1"，DOM元素可以入参为选择器也可以直接入参DOM节点比如document.getElementById('f2')）
 * @param param.offset     {Number}     偏移量 可以是数字(如:20),可以是正的也可以是负的
 * @param param.time       {Number}     指定滚动过程的时间
 * @param param.then       {Function}   滚动到指定位置时回调
 *
 * 版本：v2.0.7
 * */

const scrollTo = function(param: any = {}) {
  param.before && param.before();
  let html = document.documentElement;
  if (param.attrStatus && html) {
    html.setAttribute('data-scrolling', '1');
  }
  let { animationDistance, to, offset, time = 600, then = () => {}, speed = 16 } = param;
  const win: any = typeof window !== 'undefined' ? window : {};
  let doc = document;

  // 滚动条所在元素
  let el = isString(param.el) ? document.querySelector(param.el) : isElement(param.el) ? param.el : 'root';

  if (el && to) {
    // 滚动条开始位置
    let sta = {
      x: el === 'root' ? win.pageXOffset || doc.documentElement.scrollLeft || doc.body.scrollLeft : el.scrollLeft,
      y: el === 'root' ? win.pageYOffset || doc.documentElement.scrollTop || doc.body.scrollTop : el.scrollTop,
    };

    // 滚动条结束位置
    let end = to
      ? {
          x: isNumber(to.x) ? to.x : isElement(to.x) ? _pos(to.x, el).x : isString(to.x) ? _pos(document.querySelector(to.x), el).x : sta.x,
          y: isNumber(to.y) ? to.y : isElement(to.y) ? _pos(to.y, el).y : isString(to.y) ? _pos(document.querySelector(to.y), el).y : sta.y,
        }
      : { x: sta.x, y: sta.y };
    // console.log(end);

    // 滚动目标偏移量
    if (offset) {
      end.x += offset.x || 0;
      end.y += offset.y || 0;
    }

    // 最大滚动距离
    let _distance_x: number = Math.abs(sta.x - end.x);
    let _distance_y: number = Math.abs(sta.y - end.y);
    let _distance: number = _distance_x > _distance_y ? _distance_x : _distance_y;
    speed = !param.speed && _distance > 10000 ? 1 : 18 - (_distance / 10000) * 18;
    let _anim_pos = 0.5;
    // 设置了动画距离
    if (animationDistance) {
      time = param.time || 300;
      if (_distance_x > animationDistance) {
        sta.x = sta.x - end.x > 0 ? end.x + animationDistance : end.x - animationDistance;
        _anim_pos = 0;
      }
      if (_distance_y > animationDistance) {
        sta.y = sta.y - end.y > 0 ? end.y + animationDistance : end.y - animationDistance;
        _anim_pos = 0;
      }
    }

    // 过渡动画曲线函数
    let animate =
      isFunction(param.animate) && isNumber(param.animate(1))
        ? param.animate
        : (t: any) => (t <= _anim_pos ? speed * t * t * t * t * t : 1 + speed * --t * t * t * t * t);

    // 计算滚动条位置
    let pos = function(sta: any, end: any, elapsed: any, time: any) {
      return {
        x: elapsed > time ? end.x : sta.x + (end.x - sta.x) * animate(elapsed / time),
        y: elapsed > time ? end.y : sta.y + (end.y - sta.y) * animate(elapsed / time),
      };
    };

    // 滚动开始的时间
    let clock = new Date().getTime();

    setTimeout(() => {
      // 滚动到指定位置
      (function step() {
        let elapsed = new Date().getTime() - clock;
        if (el === 'root') {
          win.scrollTo(pos(sta, end, elapsed, time).x, pos(sta, end, elapsed, time).y);
        } else {
          el.scrollLeft = pos(sta, end, elapsed, time).x;
          el.scrollTop = pos(sta, end, elapsed, time).y;
        }

        if ((sta.x !== end.x || sta.y !== end.y) && elapsed <= time) {
          raf(step);
        } else {
          param.then && param.then(end);
          if (param.attrStatus && html) {
            html.removeAttribute('data-scrolling');
          }
          then(end);
        }
      })();
    }, 16);
  }
};
export default scrollTo;
