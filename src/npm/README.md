# MC-SCROLL

```$xslt
一个设置滚动条缓动滚动到指定位置或指定ID标签位置的原生JS插件。
```

#### 索引

- [演示](#演示)
- [参数](#参数)
- [示例](#示例)
- [版本](#版本)
- [反馈](#反馈)

#### 演示
![demo](https://plugins.fekit.cn/@fekit/scrollto/demo.gif)

[https://plugins.fekit.cn/@fekit/scrollto/](https://plugins.fekit.cn/@fekit/scrollto/)

#### 开始

下载项目:

```npm
npm i mc-scrollto
```

#### 参数

```
/**
 * 入参说明
 *
 * param.el                  {*}          指定滚动条所在的容器(可以是字符串类型的选择器比如"#idname",也可以是DOM节点document.getElementById('name'),默认不填写则为窗口滚动条。)
 * param.to                  {Obejct}     指定需要滚动的位置
 * param.animationDistance   {Number}     指定最后需要动画滚动的距离
 * param.to.x                {*}          指定需要滚动的位置（可以是数字比如100，也可以是需要滚动到的DOM元素位置比如"#f1"，DOM元素可以入参为选择器也可以直接入参DOM节点比如document.getElementById('f2')）
 * param.to.y                {*}          指定需要滚动的位置（可以是数字比如100，也可以是需要滚动到的DOM元素位置比如"#f1"，DOM元素可以入参为选择器也可以直接入参DOM节点比如document.getElementById('f2')）
 * param.offset              {Obejct}     偏移量对象
 * param.offset.x            {*}          偏移量 可以是数字(如:20),可以是正的也可以是负的
 * param.offset.y            {*}          偏移量 可以是数字(如:20),可以是正的也可以是负的
 * param.time                {Number}     指定滚动过程的时间
 * param.then                {Function}   滚动到指定位置时回调
 *
 * */
```

#### 示例

滚动到指定位置

```javascript
// 引入模块
import mcScrollTo from './mc-scrollto';

// 纵横双向滚动，并且横向位置滚动到一个指定元素的位置。纵向指定固定的数字。
mcScrollTo({
  el: '#header-title', // 滚动条容器指定为ID为header-title的元素，这里也可以写document.getElementById('header-title')
  to: {
    x: '#to_span', // 横向滚动到指定ID为to_span的标签的位置
    y: 200, // 纵向滚动到200px
  },
  offset: {
    x: 0, // 横向偏移0,可以不写
    y: 50, // 纵向偏移50px,就是200+50=250，也可以为负数
  },
  time: 600, // 滚动动画时间
});

// 滚动到指定位置,不设定el则默认为窗口的滚动条
mcScrollTo({
  to: {
    y: 630,
  },
  time: 300,
});

// 不设定el则默认为窗口的滚动条,滚动到一个class为scroll-to-f5的元素。执行一个回调并返回滚动条的目标位置
mcScrollTo({
  to: {
    y: '.scroll-to-f5',
  },
  then: function(pos) {
    alert(JSON.stringify(pos));
  },
});
```

#### 版本

```$xslt
v3.0.0 [Latest version]
1. TS 重构
2. @fekit/mc-scrollto 更名为 @fekit/scrollto
```

```$xslt
v2.1.5
1. 优化animationDistance快启慢停
```

```$xslt
v2.1.4
1. 新增speed属性从1-16，数字越小中间瞬移越快。两头动画缓动，设置为1时，基本是一启动就已到目的地。慢启-瞬移-慢停,中间瞬移自然无缝.
```

```$xslt
v2.1.0
1. 新增attrStatus状态标识，当正在滚动时，会在html标答添加一个data-scrolling="1"的标识，这个标识可用于暂停与禁止全页动画或其任务，等滚动完成后再继续.
```

```$xslt
v2.0.9
1. 新增animationDistance属性，这个属性默认为false不开启，如果开启则直接填写数字。数字是从开始位置一瞬间跳到距离目标位置还有animationDistance设定置的位置然后动画缓动滚到目标位置
```

```$xslt
v2.0.7
1. 这一版本变化较大，几乎重构了所有入参属性。
2. 移除了入参id,请使用el代替id
3. 此版本新增了可设置操作指定元素的滚动条，之前仅能操作窗口的滚动条。el属性可以入参为节点元素比如document.getElementById('xxx')，也可以是类型为字字符串的选择器比如'.xxx',也可以不写默认为窗口的滚动条。
4. 此版本新增了可操作横向滚动，之前仅支持纵向滚动。并且支持入参为固定数字，滚动到的元素位置（又可以直接写入元素document.getElementById('xxx')，也可以直接写选择器'#xxx'）
5. 优化了复杂多层定位场景下的定位准确性。
```

```$xslt
v2.0.6
1、新增功能offset偏移量，可以在指定的位置或标签ID的基础上进行正负偏移。比如滚动到ID为"F1"的标答位置但负偏移10PX或负5%；
```

```$xslt
v2.0.5
1、完成核心功能：滚动到指定ID标签的位置或滚动到指定位置-稳定版
```
